
import random
from typing import Iterator, List

from .player import BasePlayer, CautiousPlayer, DemandingPlayer, ImpulsivePlayer, RandomPlayer
from .property import Property

CREDIT_AFTER_ROUND = 100


class BoardDice:

    value: int = 1

    def throw_up(self) -> int:
        self.value = random.randint(1, 6)
        return self.value


class Board:

    round: int = 1
    dice = BoardDice()

    def __init__(self, start_player_credit: float = 100, max_propert_value: float = 150) -> None:
        self.players: List[BasePlayer] = [
            CautiousPlayer(start_player_credit),
            DemandingPlayer(start_player_credit),
            ImpulsivePlayer(start_player_credit),
            RandomPlayer(start_player_credit),
        ]
        self.items: List[Property] = list(
            map(lambda x: Property(value=random.uniform(1, max_propert_value)), range(0, 20)))

        random.shuffle(self.players)
        random.shuffle(self.items)

    @property
    def max_round(self) -> int:
        return 1000

    @property
    def players_gaming(self) -> Iterator[BasePlayer]:
        return filter(lambda player: player.is_gamming, self.players)

    @property
    def ranking(self) -> List[BasePlayer]:
        return list(sorted(self.players, key=lambda item: item.balance, reverse=True))

    @property
    def winner(self) -> BasePlayer:
        return self.ranking[0]

    def start_game(self):
        while not self.is_end_game():
            self.start_round()

        for index, player in enumerate(self.ranking):
            print(f'{index + 1}° {player} Balance: {player.balance:.2f}\n')
        print('End Game\n')

    def start_round(self):
        for player in self.players_gaming:
            print('Round:', self.round,
                  f'Player count {len(list(self.players_gaming))}')
            print(f'Player {player} is in {player.current_index}')
            index = self.player_game(player)
            print(f'Player {player} sorted {self.dice.value} Dice number')
            print(f'Player {player} is in {index}')
            item = self.items[index - 1]
            if item.owner == player:
                continue

            if item.owner:
                item.owner.receive_rent(item.rent_value)
                player.pay_rent(item.rent_value)
                print('Player:', player,
                    f'Pay {item.rent_value:.2f} of rent to {item.owner} property {index}')

            elif player.buy(item.value, item.rent_value):
                print(f'Player: {player}', f'Buy property {index}')
                item.owner=player
            print(f'Player balance: {player.balance:.2f}')

            self.items[index - 1]=item
            if not player.is_gamming:
                self.player_loser(player)
                print(f'Player {player} loser in round {self.round}')
            print('\n')
        self.round += 1

    def player_game(self, player: BasePlayer) -> int:
        dice_number: int=self.dice.throw_up()
        current_index=player.current_index + dice_number
        if player.current_index + dice_number > len(self.items):
            player.balance += CREDIT_AFTER_ROUND
            current_index=current_index - len(self.items)

        player.current_index=current_index
        return current_index

    def player_loser(self, player: BasePlayer):
        print('clear', player, self.round)
        for property in self.items:
            if property.owner == player:
                property.owner=None

    def is_end_game(self) -> bool:
        if self.round > self.max_round:
            return True
        return len(list(filter(lambda player: player.balance > 0, self.players))) == 1
