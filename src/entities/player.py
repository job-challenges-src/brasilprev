from dataclasses import dataclass
import random

@dataclass
class BasePlayer:
    balance: float
    round: int = 0
    current_index: int = 0
    victories: int = 0

    def pay_rent(self, amount: float) -> float:
        self.balance -= amount
        return amount
    
    def receive_rent(self, amount: float) -> None:
        self.balance += amount

    def buy(self, amount: float, rent_value: float) -> bool:
        # Essa regra não faz sentido junto com a de saldo negativo
        if self.balance < amount:
            return False
        raise NotImplementedError('You need implement this method')
    
    @property
    def name(self) -> str:
        return 'base'

    @property
    def is_gamming(self) -> bool:
        return self.balance >= 0


class ImpulsivePlayer(BasePlayer):

    def buy(self, amount: float, rent_value: float) -> bool:
        # Essa regra não faz sentido junto com a de saldo negativo
        if self.balance < amount:
            return False
        self.balance -= amount
        return True

    @property
    def name(self) -> str:
        return 'impulsivo'

    def __str__(self) -> str:
        return self.name

class DemandingPlayer(BasePlayer):

    def buy(self, amount: float, rent_value: float) -> bool:
        # Essa regra não faz sentido junto com a de saldo negativo
        if self.balance < amount:
            return False
        can_buy = rent_value > 50
        if can_buy:
            self.balance -= amount
        return can_buy

    @property
    def name(self) -> str:
        return 'exigente'

    def __str__(self) -> str:
        return self.name

class CautiousPlayer(BasePlayer):

    def buy(self, amount: float, rent_value: float) -> bool:
        # Essa regra não faz sentido junto com a de saldo negativo
        if self.balance < amount:
            return False
        can_buy = (self.balance - amount) >= 80
        if can_buy:
            self.balance -= amount
        return can_buy

    @property
    def name(self) -> str:
        return 'cauteloso'

    def __str__(self) -> str:
        return self.name

class RandomPlayer(BasePlayer):

    def buy(self, amount: float, rent_value: float) -> bool:
        # Essa regra não faz sentido junto com a de saldo negativo
        if self.balance < amount:
            return False
        can_buy = bool(random.randint(0,1))
        if can_buy:
            self.balance -= amount
        return can_buy

    @property
    def name(self) -> str:
        return 'aleatório'

    def __str__(self) -> str:
        return self.name