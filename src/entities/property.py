

from dataclasses import dataclass
from typing import Optional

from .player import BasePlayer

@dataclass
class Property:
    value: float
    rent_percent: float = 30
    owner: Optional[BasePlayer] = None

    @property
    def rent_value(self) -> float:
        return self.value * (self.rent_percent/ 100)