from typing import List
from src.entities.board import Board
from collections import Counter
import statistics

games: List[Board] = list(map(lambda _: Board(), range(0, 300)))

for index, game in enumerate(games):
    print(f'{"#" *10} Starting game {index + 1} {"#" *10}\n\n')
    game.start_game()



print(f'{"#" * 15} Result {"#" * 15}', '\n')
print(f'Quantas partidas terminam portime out (1000 rodadas);\nR: {len(list(filter(lambda i: i.round >= i.max_round, games)))}', '\n')
print(f'Quantos turnos em média demora uma partida;\nR: {statistics.mean(list(map(lambda game: game.round, games))):.2f}', '\n')
print(f'Qual a porcentagem de vitórias por comportamento dos jogadores;')
results = sorted(Counter(list(map(lambda game: game.winner.name, games))).items(), key=lambda x: x[1], reverse=True)
for player, winners in results:
    print('\t',player, f'{(winners * 100) / len(games):.2f}%')
print(f'\nQual o comportamento que mais vence\nR: {results[0][0]}')